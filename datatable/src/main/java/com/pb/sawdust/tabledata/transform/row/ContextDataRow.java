package com.pb.sawdust.tabledata.transform.row;

import com.pb.sawdust.tabledata.DataRow;

/**
 * The {@code ContextDataRow} is a data row associated with a particular "context." Basically, this interface allows a
 * data row to be a simple container holding some informational object which can be accessed.
 *
 * @param <C>
 *        The type of the context held by this data row.
 *
 * @author crf
 *         Started 1/24/12 5:19 PM
 */
public interface ContextDataRow<C> extends DataRow {
    /**
     * Get the context associated with this data row.
     *
     * @return this data row's context.
     */
    C getContext();
}
