package com.pb.sawdust.tabledata.transform;

/**
 * The {@code WrappingDataTableTransformation} interface is used to specify a {@code DataTableTransformation} which does
 * not modify the input table directly. Often, this is carried out by providing a mutating "view" to the input table as a
 * wrapped data table.
 *
 * @author crf
 *         Started 1/20/12 6:07 AM
 */
public interface WrappingDataTableTransformation extends DataTableTransformation {
}
