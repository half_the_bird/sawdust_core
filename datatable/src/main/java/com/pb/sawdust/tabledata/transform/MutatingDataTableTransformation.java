package com.pb.sawdust.tabledata.transform;

import com.pb.sawdust.tabledata.DataTable;

/**
 * The {@code MutatingDataTableTransformation} interface is used to specify a {@code DataTableTransformation} which modifies
 * the input table directly.
 *
 * @author crf
 *         Started 1/20/12 6:06 AM
 */
public interface MutatingDataTableTransformation extends DataTableTransformation {

    /**
     * {@inheritDoc}
     *
     * @return the input table, after it has been transformed.
     */
    DataTable transform(DataTable table);
}
