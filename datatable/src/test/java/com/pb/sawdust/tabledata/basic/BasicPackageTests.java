package com.pb.sawdust.tabledata.basic;


/**
 * @author crf <br/>
 *         Started: Sep 24, 2008 5:26:59 PM
 */
public class BasicPackageTests {

    public static void main(String ... args) {
        BasicDataRowTest.main();
        RowDataTableTest.main();
        ColumnDataTableTest.main();
        ListDataTableTest.main();
        BasicDataSetTest.main();
        BasicTableIndexTest.main();
        DigestTableIndexTest.main();
        BasicDataColumnTest.main();
    }
}
