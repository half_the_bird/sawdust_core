package com.pb.sawdust.tabledata;

import com.pb.sawdust.tabledata.basic.BasicPackageTests;

/**
 * @author crf <br/>
 *         Started: Sep 24, 2008 5:34:48 PM
 */
public class TableDataPackageTests {
    public static void main(String ... args) {
        BasicPackageTests.main();
    }
}
