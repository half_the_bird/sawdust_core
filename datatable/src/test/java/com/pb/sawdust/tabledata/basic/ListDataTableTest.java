package com.pb.sawdust.tabledata.basic;

import com.pb.sawdust.tabledata.DataTable;
import com.pb.sawdust.tabledata.AbstractDataTableTest;
import com.pb.sawdust.tabledata.metadata.TableSchema;
import com.pb.sawdust.util.test.TestBase;
import org.junit.Test;

/**
 * @author crf <br/>
 *         Started: Sep 24, 2008 5:12:18 PM
 */
public class ListDataTableTest extends AbstractDataTableTest {

    public static void main(String ... args) {
        TestBase.main();
    }

    protected DataTable getDataTable(Object[][] tableData, TableSchema schema) {
        return new ListDataTable(schema,tableData);
    }
    
    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumn() {
        super.testAddColumn();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnCount() {
        super.testAddColumnCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnLabel() {
        super.testAddColumnLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnFailure() {
        super.testAddColumnFailure();
    }


    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnWrongType() {
        super.testAddColumnWrongType();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowData() {
        super.testSetRowData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataRowTooLow() {
        super.testSetRowDataRowTooLow();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataRowTooHigh() {
        super.testSetRowDataRowTooHigh();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataWrongColumnCount() {
        super.testSetRowDataWrongColumnCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataWrongDataType() {
        super.testSetRowDataWrongDataType();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataByKey() {
        super.testSetRowDataByKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataRowByKeyInvalidKey() {
        super.testSetRowDataRowByKeyInvalidKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataByKeyWrongColumnCount() {
        super.testSetRowDataByKeyWrongColumnCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataByKeyWrongDataType() {
        super.testSetRowDataByKeyWrongDataType();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnData() {
        super.testSetColumnData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnDataNotArray() {
        super.testSetColumnDataNotArray();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnDataInvalidData() {
        super.testSetColumnDataInvalidData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnDataLabel() {
        super.testSetColumnDataLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnDataLabelNotArray() {
        super.testSetColumnDataLabelNotArray();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnDataLabelWrongArraySize() {
        super.testSetColumnDataLabelWrongArraySize();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnDataLabelInvalidData() {
        super.testSetColumnDataLabelInvalidData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValue() {
        super.testSetCellValue();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueRowTooLow() {
        super.testSetCellValueRowTooLow();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueRowTooHigh() {
        super.testSetCellValueRowTooHigh();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueColumnTooLow() {
        super.testSetCellValueColumnTooLow();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueColumnTooHigh() {
        super.testSetCellValueColumnTooHigh();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueInvalidData() {
        super.testSetCellValueInvalidData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabel() {
        super.testSetCellValueLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelRowTooLow() {
        super.testSetCellValueLabelRowTooLow();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelRowTooHigh() {
        super.testSetCellValueLabelRowTooHigh();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueInvalidLabel() {
        super.testSetCellValueInvalidLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelInvalidData() {
        super.testSetCellValueLabelInvalidData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueByKey() {
        super.testSetCellValueByKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueByInvalidKey() {
        super.testSetCellValueByInvalidKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueByKeyColumnTooLow() {
        super.testSetCellValueByKeyColumnTooLow();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueByKeyColumnTooHigh() {
        super.testSetCellValueByKeyColumnTooHigh();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueByKeyInvalidData() {
        super.testSetCellValueByKeyInvalidData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelByKey() {
        super.testSetCellValueLabelByKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelByInvalidKey() {
        super.testSetCellValueLabelByInvalidKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueInvalidLabelByKey() {
        super.testSetCellValueInvalidLabelByKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelByKeyInvalidData() {
        super.testSetCellValueLabelByKeyInvalidData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataByKeyColumn() {
        super.testSetRowDataByKeyColumn();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnReturn() {
        super.testAddColumnReturn();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueInDataFailure() {
        super.testSetCellValueInDataFailure();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueInData() {
        super.testSetCellValueInData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void deleteColumnFromDataCount() {
        super.deleteColumnFromDataCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void deleteColumnFromData() {
        super.deleteColumnFromData();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnToDataReturn() {
        super.testAddColumnToDataReturn();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testAddColumnToDataCount() {
        super.testAddColumnToDataCount();
    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowReturn() {
//        super.testDeleteRowReturn();
//    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowCount() {
//        super.testDeleteRowCount();
//    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowFailureTooLow() {
//        super.testDeleteRowFailureTooLow();
//    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowFailureTooHigh() {
//        super.testDeleteRowFailureTooHigh();
//    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowByKeyReturn() {
//        super.testDeleteRowByKeyReturn();
//    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowByKeyCount() {
//        super.testDeleteRowByKeyCount();
//    }
//
//    @Test(expected=UnsupportedOperationException.class)
//    public void testDeleteRowByKeyFailureTooLow() {
//        super.testDeleteRowByKeyFailureTooLow();
//    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnReturn() {
        super.testDeleteColumnReturn();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnCount() {
        super.testDeleteColumnCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabel() {
        super.testDeleteColumnLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnSchemaCount() {
        super.testDeleteColumnSchemaCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnSchemaLabel() {
        super.testDeleteColumnSchemaLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnFailureTooLow() {
        super.testDeleteColumnFailureTooLow();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnFailureTooHigh() {
        super.testDeleteColumnFailureTooHigh();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabelReturn() {
        super.testDeleteColumnLabelReturn();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabelCount() {
        super.testDeleteColumnLabelCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabelLabel() {
        super.testDeleteColumnLabelLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabelSchemaCount() {
        super.testDeleteColumnLabelSchemaCount();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabelSchemaLabel() {
        super.testDeleteColumnLabelSchemaLabel();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testDeleteColumnLabelFailure() {
        super.testDeleteColumnLabelFailure();
    }

}
