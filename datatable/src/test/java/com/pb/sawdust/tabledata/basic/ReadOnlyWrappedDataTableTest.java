package com.pb.sawdust.tabledata.basic;

import org.junit.Ignore;
import org.junit.Test;

/**
 * The {@code ReadOnlyWrappedDataTableTest} ...
 *
 * @author crf
 *         Started 9/30/11 2:15 PM
 */
public abstract class ReadOnlyWrappedDataTableTest extends FixedSizeDataTableTest {

    @Test(expected=UnsupportedOperationException.class)
    public void testSetPrimaryKey() {
        super.testSetPrimaryKey();
    }

    @Test(expected=UnsupportedOperationException.class)
    public void testSetPrimaryKeyByColumn() {
        super.testSetPrimaryKeyByColumn();
    }

    @Test @Ignore
    public void testSetPrimaryKeyColumnDoesntExist() {}

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowData() {
        super.testSetRowData();
    }

    @Test @Ignore
    public void testSetRowDataRowTooLow() {}

    @Test @Ignore
    public void testSetRowDataRowTooHigh() {}

    @Test @Ignore
    public void testSetRowDataWrongColumnCount() {}

    @Test @Ignore
    public void testSetRowDataWrongDataType() {}

    @Test(expected=UnsupportedOperationException.class)
    public void testSetRowDataByKey() {
        super.testSetRowDataByKey();
    }

    @Test @Ignore
    public void testSetRowDataByKeyColumn() {}

    @Test @Ignore
    public void testSetRowDataRowByKeyInvalidKey() {}

    @Test @Ignore
    public void testSetRowDataByKeyWrongColumnCount() {}

    @Test @Ignore
    public void testSetRowDataByKeyWrongDataType() {}

    @Test(expected=UnsupportedOperationException.class)
    public void testSetColumnData() {
        super.testSetColumnData();
    }

    @Test @Ignore
    public void testSetColumnDataNotArray() {}

    @Test @Ignore
    public void testSetColumnDataInvalidData() {}

    @Test @Ignore
    public void testSetColumnDataLabel() {}

    @Test @Ignore
    public void testSetColumnDataLabelNotArray() {}

    @Test @Ignore
    public void testSetColumnDataLabelWrongArraySize() {}

    @Test @Ignore
    public void testSetColumnDataLabelInvalidData() {}

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValue() {
        super.testSetCellValue();
    }

    @Test @Ignore
    public void testSetCellValueRowTooLow() {}

    @Test @Ignore
    public void testSetCellValueRowTooHigh() {}

    @Test @Ignore
    public void testSetCellValueColumnTooLow() {}

    @Test @Ignore
    public void testSetCellValueColumnTooHigh() {}

    @Test @Ignore
    public void testSetCellValueInvalidData() {}

    @Test @Ignore
    public void testSetCellValueLabel() {}

    @Test @Ignore
    public void testSetCellValueLabelRowTooLow() {}

    @Test @Ignore
    public void testSetCellValueLabelRowTooHigh() {}

    @Test @Ignore
    public void testSetCellValueInvalidLabel() {}

    @Test @Ignore
    public void testSetCellValueLabelInvalidData() {}

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueByKey() {
        super.testSetCellValueByKey();
    }

    @Test @Ignore
    public void testSetCellValueByInvalidKey() {}

    @Test @Ignore
    public void testSetCellValueByKeyColumnTooLow() {}

    @Test @Ignore
    public void testSetCellValueByKeyColumnTooHigh() {}

    @Test @Ignore
    public void testSetCellValueByKeyInvalidData() {}

    @Test(expected=UnsupportedOperationException.class)
    public void testSetCellValueLabelByKey() {
        super.testSetCellValueLabelByKey();
    }

    @Test @Ignore
    public void testSetCellValueLabelByInvalidKey() {}

    @Test @Ignore
    public void testSetCellValueInvalidLabelByKey() {}

    @Test @Ignore
    public void testSetCellValueLabelByKeyInvalidData() {}

    @Ignore @Test
    public void testGetRowByKey() {}

    @Ignore @Test
    public void testGetCellValueByKey() {}

    @Ignore @Test
    public void testGetCellValueByKeyFailure1() {}

    @Ignore @Test
    public void testGetCellValueByKeyFailure2() {}

    @Ignore @Test
    public void testGetCellValueByKeyFailure3() {}
}
