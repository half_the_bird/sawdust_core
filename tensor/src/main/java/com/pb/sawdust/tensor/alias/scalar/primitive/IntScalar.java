package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.IntD0Tensor;

/**
 * The {@code IntScalar} interface provides a {@code Scalar} which holds a {@code int}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface IntScalar extends IntD0Tensor {
}
