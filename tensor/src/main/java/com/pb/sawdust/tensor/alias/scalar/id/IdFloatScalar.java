package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.FloatScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdFloatD0Tensor;

/**
 * The {@code IdFloatScalar} interface provides a {@code IdScalar} which holds a {@code float}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdFloatScalar<I> extends IdFloatD0Tensor<I>,FloatScalar {
}
