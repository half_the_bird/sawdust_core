package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.LongD0Tensor;

/**
 * The {@code LongScalar} interface provides a {@code Scalar} which holds a {@code long}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface LongScalar extends LongD0Tensor {
}
