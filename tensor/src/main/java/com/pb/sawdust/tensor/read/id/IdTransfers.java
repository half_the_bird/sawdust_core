package com.pb.sawdust.tensor.read.id;

/**
 * The {@code IdTransfers} provides some convenient, standard {@code IdTransfer} instances.
 *
 * @author crf <br/>
 *         Started 1/24/11 3:41 PM
 */
public class IdTransfers {
    private IdTransfers() {}

    /**
     * An id transfer from {@code String}s to {@code byte}s.
     */
    public static final IdTransfer<String,Byte> STRING_TO_BYTE_TRANSFER = new IdTransfer<String,Byte>() {
        @Override
        public Byte getId(String sourceId) {
            return Byte.valueOf(sourceId);
        }

        @Override
        public Class<Byte> getIdClass() {
            return Byte.class;
        }
    }; 

    /**
     * An id transfer from {@code String}s to {@code short}s.
     */
    public static final IdTransfer<String,Short> STRING_TO_SHORT_TRANSFER = new IdTransfer<String, Short>() {
        @Override
        public Short getId(String sourceId) {
            return Short.valueOf(sourceId);
        }

        @Override
        public Class<Short> getIdClass() {
            return Short.class;
        }
    };

    /**
     * An id transfer from {@code String}s to {@code int}s.
     */
    public static final IdTransfer<String,Integer> STRING_TO_INT_TRANSFER = new IdTransfer<String,Integer>() {
        @Override
        public Integer getId(String sourceId) {
            return Integer.valueOf(sourceId);
        }

        @Override
        public Class<Integer> getIdClass() {
            return Integer.class;
        }
    };

    /**
     * An id transfer from {@code String}s to {@code long}s.
     */
    public static final IdTransfer<String,Long> STRING_TO_LONG_TRANSFER = new IdTransfer<String, Long>() {
        @Override
        public Long getId(String sourceId) {
            return Long.valueOf(sourceId);
        }

        @Override
        public Class<Long> getIdClass() {
            return Long.class;
        }
    };

    /**
     * An id transfer from {@code String}s to {@code float}s.
     */
    public static final IdTransfer<String,Float> STRING_TO_FLOAT_TRANSFER = new IdTransfer<String, Float>() {
        @Override
        public Float getId(String sourceId) {
            return Float.valueOf(sourceId);
        }

        @Override
        public Class<Float> getIdClass() {
            return Float.class;
        }
    };

    /**
     * An id transfer from {@code String}s to {@code double}s.
     */
    public static final IdTransfer<String,Double> STRING_TO_DOUBLE_TRANSFER = new IdTransfer<String, Double>() {
        @Override
        public Double getId(String sourceId) {
            return Double.valueOf(sourceId);
        }

        @Override
        public Class<Double> getIdClass() {
            return Double.class;
        }
    };
}
