package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.LongScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdLongD0Tensor;

/**
 * The {@code IdLongScalar} interface provides a {@code IdScalar} which holds a {@code long}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdLongScalar<I> extends IdLongD0Tensor<I>,LongScalar {
}
