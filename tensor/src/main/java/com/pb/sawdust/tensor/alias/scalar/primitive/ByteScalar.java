package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.ByteD0Tensor;

/**
 * The {@code ByteScalar} interface provides a {@code Scalar} which holds a {@code byte}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface ByteScalar extends ByteD0Tensor {
}
