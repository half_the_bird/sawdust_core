package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.CharD0Tensor;

/**
 * The {@code CharScalar} interface provides a {@code Scalar} which holds a {@code char}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface CharScalar extends CharD0Tensor {
}
