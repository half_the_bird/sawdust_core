package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.IntScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdIntD0Tensor;

/**
 * The {@code IdIntScalar} interface provides a {@code IdScalar} which holds a {@code int}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdIntScalar<I> extends IdIntD0Tensor<I>,IntScalar {
}
