package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.ByteScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdByteD0Tensor;

/**
 * The {@code IdByteScalar} interface provides a {@code IdScalar} which holds a {@code byte}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdByteScalar<I> extends IdByteD0Tensor<I>,ByteScalar {
}
