package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.DoubleD0Tensor;

/**
 * The {@code DoubleScalar} interface provides a {@code Scalar} which holds a {@code double}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface DoubleScalar extends DoubleD0Tensor {
}
