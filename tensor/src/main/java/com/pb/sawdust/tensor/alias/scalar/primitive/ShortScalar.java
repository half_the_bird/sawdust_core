package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.ShortD0Tensor;

/**
 * The {@code ShortScalar} interface provides a {@code Scalar} which holds a {@code short}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface ShortScalar extends ShortD0Tensor {
}
