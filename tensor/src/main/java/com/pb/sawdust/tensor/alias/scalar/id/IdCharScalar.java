package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.CharScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdCharD0Tensor;

/**
 * The {@code IdCharScalar} interface provides a {@code IdScalar} which holds a {@code char}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdCharScalar<I> extends IdCharD0Tensor<I>,CharScalar {
}
