package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.FloatD0Tensor;

/**
 * The {@code FloatScalar} interface provides a {@code Scalar} which holds a {@code float}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface FloatScalar extends FloatD0Tensor {
}
