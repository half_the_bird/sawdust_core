package com.pb.sawdust.tensor.alias.scalar.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.BooleanD0Tensor;

/**
 * The {@code BooleanScalar} interface provides a {@code Scalar} which holds a {@code boolean}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 8:14:56 PM
 */
public interface BooleanScalar extends BooleanD0Tensor {
}
