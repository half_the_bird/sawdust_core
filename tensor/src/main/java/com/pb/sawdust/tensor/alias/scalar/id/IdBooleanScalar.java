package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.BooleanScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdBooleanD0Tensor;

/**
 * The {@code IdBooleanScalar} interface provides a {@code IdScalar} which holds a {@code boolean}.
 *
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdBooleanScalar<I> extends IdBooleanD0Tensor<I>,BooleanScalar {
}
