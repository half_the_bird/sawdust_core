package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.DoubleScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdDoubleD0Tensor;

/**
 * The {@code IdDoubleScalar} interface provides a {@code IdScalar} which holds a {@code double}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdDoubleScalar<I> extends IdDoubleD0Tensor<I>,DoubleScalar {
}
