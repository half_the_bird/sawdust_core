package com.pb.sawdust.tensor.alias.scalar.id;

import com.pb.sawdust.tensor.alias.scalar.primitive.ShortScalar;
import com.pb.sawdust.tensor.decorators.id.primitive.size.IdShortD0Tensor;

/**
 * The {@code IdShortScalar} interface provides a {@code IdScalar} which holds a {@code short}.
 * 
 * @author crf <br/>
 *         Started: Jun 16, 2009 11:39:14 PM
 */
public interface IdShortScalar<I> extends IdShortD0Tensor<I>,ShortScalar {
}
