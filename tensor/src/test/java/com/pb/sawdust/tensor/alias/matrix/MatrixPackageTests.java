package com.pb.sawdust.tensor.alias.matrix;

import com.pb.sawdust.tensor.alias.matrix.primitive.PrimitivePackageTests;
import com.pb.sawdust.tensor.alias.matrix.id.IdPackageTests;

/**
 * @author crf <br/>
 *         Started: Jul 5, 2009 11:01:48 AM
 */
public class MatrixPackageTests {
    public static void main(String ... args)  {
        PrimitivePackageTests.main();
        IdPackageTests.main();
    }
}
