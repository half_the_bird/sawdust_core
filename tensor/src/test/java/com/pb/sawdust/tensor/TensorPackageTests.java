package com.pb.sawdust.tensor;

import com.pb.sawdust.tensor.decorators.DecoratorsPackageTests;
import com.pb.sawdust.tensor.alias.AliasPackageTests;
import com.pb.sawdust.tensor.factory.FactoryPackageTests;
import com.pb.sawdust.tensor.index.IndexPackageTests;
import com.pb.sawdust.tensor.slice.SlicePackageTests;

/**
 * @author crf <br/>
 *         Started: Jul 4, 2009 6:08:34 PM
 */
public class TensorPackageTests {
    public static void main(String ... args)  {
        ArrayTensorTest.main();
        LinearTensorTest.main();
        SparseTensorTest.main();
        AliasPackageTests.main();
        DecoratorsPackageTests.main();
        FactoryPackageTests.main();
        IndexPackageTests.main();
        SlicePackageTests.main();
    }
}
