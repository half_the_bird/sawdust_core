package com.pb.sawdust.tensor.slice;

/**
 * @author crf <br/>
 *         Started: Feb 27, 2009 4:43:03 PM
 */
public class SlicePackageTests {
    public static void main(String ... args) {
        BaseSliceTest.main();
        FullSliceTest.main();
        ReducingSliceTest.main();
        CompositeSliceTest.main();
        SliceUtilTest.main();
    }
}
