package com.pb.sawdust.tensor.decorators.id.primitive;

import com.pb.sawdust.tensor.decorators.id.primitive.size.SizePackageTests;

/**
 * @author crf <br/>
 *         Started: Jul 24, 2009 12:24:05 AM
 */
public class PrimitivePackageTests {  
    public static void main(String ... args) {
        SizePackageTests.main();
    }
}
