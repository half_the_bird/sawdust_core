package com.pb.sawdust.tensor.index;

/**
 * @author crf <br/>
 *         Started: Feb 18, 2009 3:35:35 PM
 */
public class IndexPackageTests {
    public static void main(String ... args) {
        BaseIndexTest.main();
        CollapsingIndexTest.main();
        IndexUtilTest.main();
        StandardIndexTest.main();
        SliceIndexTest.main();
        ScalarIndexTest.main();
    }
}
