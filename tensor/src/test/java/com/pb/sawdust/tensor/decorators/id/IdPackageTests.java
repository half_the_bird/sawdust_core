package com.pb.sawdust.tensor.decorators.id;

import com.pb.sawdust.tensor.decorators.id.size.SizePackageTests;
import com.pb.sawdust.tensor.decorators.id.primitive.PrimitivePackageTests;

/**
 * @author crf <br/>
 *         Started: Jan 26, 2009 12:10:12 PM
 */
public class IdPackageTests {
    public static void main(String ... args) {
        SizePackageTests.main();
        PrimitivePackageTests.main();
    }
}
