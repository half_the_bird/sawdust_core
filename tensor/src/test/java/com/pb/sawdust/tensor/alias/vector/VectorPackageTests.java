package com.pb.sawdust.tensor.alias.vector;

import com.pb.sawdust.tensor.alias.vector.primitive.PrimitivePackageTests;
import com.pb.sawdust.tensor.alias.vector.id.IdPackageTests;

/**
 * @author crf <br/>
 *         Started: Jul 5, 2009 10:58:41 AM
 */
public class VectorPackageTests {      
    public static void main(String ... args)  {
        PrimitivePackageTests.main();
        IdPackageTests.main();
    }
}
