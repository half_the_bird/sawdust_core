package com.pb.sawdust.tensor.decorators.primitive;

import com.pb.sawdust.tensor.decorators.primitive.size.SizePackageTests;

/**
 * @author crf <br/>
 *         Started: Jan 26, 2009 4:21:44 PM
 */
public class PrimitivePackageTests {
    public static void main(String ... args) {
        SizePackageTests.main();
    }
}
