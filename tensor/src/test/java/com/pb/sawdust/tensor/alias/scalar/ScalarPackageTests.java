package com.pb.sawdust.tensor.alias.scalar;

import com.pb.sawdust.tensor.alias.scalar.primitive.PrimitivePackageTests;
import com.pb.sawdust.tensor.alias.scalar.id.IdPackageTests;

/**
 * @author crf <br/>
 *         Started: Jul 5, 2009 2:12:35 PM
 */
public class ScalarPackageTests {
    public static void main(String ... args)  {
        PrimitivePackageTests.main();
        IdPackageTests.main();
    }
}
