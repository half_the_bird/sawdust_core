package com.pb.sawdust.geography;

/**
 * The {@code IdGeographyElement} is a simple {@code GeographicElement} implementation which uses an integer for its identifier.
 *
 * @author crf
 *         Started 10/17/11 11:06 AM
 */
public class IdGeographyElement extends AbstractGeographyElement<Integer> {

    /**
     * Constructor specifying the identifier, size, and description for the element.
     *
     * @param id
     *        The element's identifier.
     *
     * @param size
     *        The element's size.
     *
     * @param description
     *        The element's description.
     */
    public IdGeographyElement(Integer id, double size, String description) {
        super(id,size,description);
    }

    /**
     * Constructor specifying the identifier and size for the element. An empty string will be used for the description.
     *
     * @param id
     *        The element's identifier.
     *
     * @param size
     *        The element's size.
     */
    public IdGeographyElement(Integer id, double size) {
        this(id,size,"");
    }
}
