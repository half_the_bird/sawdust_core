package com.pb.sawdust.calculator.tensor.la.mm;

/**
 * The {@code MatrixMultiplication} interface combines the various matrix multiplication interfaces acting on matrices and
 * vectors.
 *
 * @author crf <br/>
 *         Started Dec 13, 2010 6:52:47 AM
 */
public interface MatrixMultiplication extends VectorVectorMultiplication,MatrixVectorMultiplication,MatrixMatrixMultiplication {
}
