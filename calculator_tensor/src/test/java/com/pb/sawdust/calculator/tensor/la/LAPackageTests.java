package com.pb.sawdust.calculator.tensor.la;

import com.pb.sawdust.calculator.tensor.la.mm.MMPackageTests;

/**
 * The {@code LAPackageTests} ...
 *
 * @author crf <br/>
 *         Started Dec 14, 2010 7:52:41 AM
 */
public class LAPackageTests {
    public static void main(String ... args) {
        MMPackageTests.main();
    }
}
