package com.pb.sawdust.calculator.tensor;

import com.pb.sawdust.calculator.tensor.la.LAPackageTests;

/**
 * The {@code TensorPackageTests} ...
 *
 * @author crf <br/>
 *         Started Jul 11, 2010 10:17:02 PM
 */
public class TensorPackageTests {  
    public static void main(String ... args) {
        DefaultCellWiseTensorCalculationTest.main();
        LAPackageTests.main();
    }
}
