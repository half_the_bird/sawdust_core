package com.pb.sawdust.calculator.tensor.la.mm;

/**
 * The {@code MMPackageTests} ...
 *
 * @author crf <br/>
 *         Started Dec 13, 2010 9:35:59 AM
 */
public class MMPackageTests {
    public static void main(String ... args) {
        DefaultMatrixMultiplicationTest.main();
    }
}
