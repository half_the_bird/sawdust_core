package com.pb.sawdust.calculator;

import java.util.List;

/**
 * The {@code ParameterizedNumericFunction} ...
 *
 * @author crf
 *         Started 4/3/12 4:40 PM
 */
public interface ParameterizedNumericFunction {
    NumericFunctionN getFunction();

    List<String> getArgumentNames();
}
