package com.pb.sawdust.calculator;


/**
 * The {@code NumericResultFunction2} specifies a two-argument function which returns a numeric value as its result.
 *
 * @author crf
 *         Started 1/26/12 11:55 AM
 */
public interface NumericResultFunction2<X,Y> extends Function2<X,Y,Number> {
    /**
     * Apply the function to get a {@code byte} result.
     *
     * @param x
     *        The first argument.
     *
     * @param y
     *        The second argument.
     *
     * @return the result of the function, applied to {@code x} and {@code y}.
     */
    byte applyByte(X x, Y y);

    /**
     * Apply the function to get a {@code short} result.
     *
     * @param x
     *        The first argument.
     *
     * @param y
     *        The second argument.
     *
     * @return the result of the function, applied to {@code x} and {@code y}.
     */
    short applyShort(X x, Y y);

    /**
     * Apply the function to get an {@code int} result.
     *
     * @param x
     *        The first argument.
     *
     * @param y
     *        The second argument.
     *
     * @return the result of the function, applied to {@code x} and {@code y}.
     */
    int applyInt(X x, Y y);

    /**
     * Apply the function to get a {@code long} result.
     *
     * @param x
     *        The first argument.
     *
     * @param y
     *        The second argument.
     *
     * @return the result of the function, applied to {@code x} and {@code y}.
     */
    long applyLong(X x, Y y);

    /**
     * Apply the function to get a {@code float} result.
     *
     * @param x
     *        The first argument.
     *
     * @param y
     *        The second argument.
     *
     * @return the result of the function, applied to {@code x} and {@code y}.
     */
    float applyFloat(X x, Y y);

    /**
     * Apply the function to get a {@code double} result.
     *
     * @param x
     *        The first argument.
     *
     * @param y
     *        The second argument.
     *
     * @return the result of the function, applied to {@code x} and {@code y}.
     */
    double applyDouble(X x, Y y);    
}
