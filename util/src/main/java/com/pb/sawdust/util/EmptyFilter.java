package com.pb.sawdust.util;

/**
 * The {@code EmptyFilter} is a filter which passes everything. That is, its {@code filter(T)} method always returns {@code true}.
 *
 * @param <T>
 *        The type this filter acts on.
 *
 * @author crf
 *         Started 10/13/11 6:29 PM
 */
public class EmptyFilter<T> implements Filter<T> {
    @Override
    public boolean filter(T input) {
        return true;
    }
}
