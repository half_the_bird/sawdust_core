package com.pb.sawdust.util;

/**
 * The {@code Filter} interface provides a framework for filtering objects based on some standard.
 *
 * @param <I>
 *        The type of input which this class filters.
 *
 * @author crf <br/>
 *         Started: Jul 9, 2008 4:10:42 PM
 */
public interface Filter<I> {

    /**
     * Filter the given input. A return value of {@code true} indicates the input has passed the filter, while a
     * value of {@code false} indicates rejection.
     *
     * @param input
     *        The input to be filtered.
     *
     * @return {@code true} if {@code input} passes the filter, {@code false} otherwise.
     */
    boolean filter(I input);
}
