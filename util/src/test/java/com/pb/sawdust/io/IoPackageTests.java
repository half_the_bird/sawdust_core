package com.pb.sawdust.io;

/**
 * The {@code IoPackageTests} ...
 *
 * @author crf <br/>
 *         Started Mar 21, 2010 10:27:23 PM
 */
public class IoPackageTests {
    public static void main(String ... args) {
        TextFileTest.main();
        ZipFileTest.main();
        IterableFileReaderTest.main();
        DelimitedDataReaderTest.main();
    }
}
