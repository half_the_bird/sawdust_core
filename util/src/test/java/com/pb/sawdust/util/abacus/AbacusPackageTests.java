package com.pb.sawdust.util.abacus;

/**
 * The {@code AbacusPackageTests} ...
 *
 * @author crf <br/>
 *         Started 5/12/11 7:31 AM
 */
public class AbacusPackageTests {
    public static void main(String ... args) {
        AbacusTest.main();
        LockableAbacusTest.main();
        LockShiftingAbacusTest.main();
        RollingAbacusTest.main();
    }
}
