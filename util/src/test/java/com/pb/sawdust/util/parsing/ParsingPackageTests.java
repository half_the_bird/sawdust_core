package com.pb.sawdust.util.parsing;


/**
 * @author crf <br/>
 *         Started: Sep 4, 2008 11:57:21 PM
 */
public class ParsingPackageTests {

    public static void main(String ... args) {
        BinaryParserTest.main();
        FixedWidthStringParserTest.main();
        ObjectArrayParserTest.main();
        DelimitedDataParserTest.main();
    }
}
